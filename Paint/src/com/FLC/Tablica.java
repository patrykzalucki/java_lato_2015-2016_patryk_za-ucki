package com.FLC;
import javafx.scene.canvas.*;
import org.w3c.dom.events.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Canvas;
import java.awt.event.*;
import java.awt.event.MouseEvent;
import java.awt.peer.CanvasPeer;
import java.util.ArrayList;
import java.awt.geom.*;
import java.util.Iterator;

public class Tablica extends Canvas implements MouseListener {


    Shape aktualnyPrymityw = null;
    Point poczatekRysowania, koniecRysowania;


    Tablica() {

        this.addMouseListener(new MouseAdapter() {

            public void mousePressed(MouseEvent e) {
                poczatekRysowania = new Point(e.getX(), e.getY());
                koniecRysowania = poczatekRysowania;
                repaint();
            }

            public void mouseReleased(MouseEvent e) {

                //rysowanie linii - reszta ksztaltow bedzie zalezec od zmiennej

                koniecRysowania = new Point(e.getX(), e.getY());

                //TODO: dodac ifa odpowiedzialnego za obsluge innych ksztaltow;

                aktualnyPrymityw = narysujLinie(poczatekRysowania.x, poczatekRysowania.y, koniecRysowania.x, koniecRysowania.y);

                repaint();
            }

        });
}

    public void paint(Graphics g) {

        Graphics2D ustawieniaGrafiki = (Graphics2D) g;
            ustawieniaGrafiki.draw(aktualnyPrymityw);
    }



    private Line2D.Float narysujLinie (int x1, int y1, int x2, int y2)
    {
        return new Line2D.Float (x1, y1, x2, y2);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}


